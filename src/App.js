
import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FaCheck, FaPen, FaTrash } from "react-icons/fa";

import './App.css';
function App() {
  {/* Task (To DO List) State */ }
  const [toDo, setToDo] = useState([
    { "id": 1, "title": "Task 1", "status": "false" },
    { "id": 2, "title": "Task 2", "status": "false" }
  ]);

  {/* Temp State */ }
  const [newTask, setNewTask] = useState('');
  const [updateData, setUpdateData] = useState('');


  {/* Add Task */ }
  const addTask = () => {
    if (newTask) {
      let num = toDo.length + 1;
      let newEntry = { id: num, title: newTask, status: false }
      setToDo([...toDo, newEntry])
      setNewTask('');
    }
  }

  {/* Delete Task */ }
  const deleteTask = (id) => {
    let newTask = toDo.filter(task => task.id !== id)
    setToDo(newTask);
  }

  {/* Mark Task as done or completed */ }
  const markDone = (id) => {
    let newTask = toDo.map(task => {
      if (task.id === id) {
        return ({ ...task, status: !task.status })
      }
      return task;
    })
    setToDo(newTask);
  }


  {/* Cancel Update*/ }
  const cancelUpdate = () => {
    setUpdateData('');
  }


  {/* Change task for update*/ }
  const changeTask = (e) => {
    let newEntry = {
      id: updateData.id,
      title: e.target.value,
      status: updateData.status ? true : false
    }
    setUpdateData(newEntry);
  }


  {/* Update task*/ }
  const updateTask = () => {
    let filterRecords = [...toDo].filter(task => task.id !== updateData.id);
    let updatedObject = [...filterRecords, updateData]
    setToDo(updatedObject);
    setUpdateData('');
  }


  return (
    <div className="container App">

      <br></br>
      <h2>To Do List App</h2>
      <br></br>

      {/* Update Task */}
      {updateData && updateData ? (
        <>
          <div className='row'>
            <div className='col'>
              <input
                value={updateData && updateData.title}
                onChange={(e) => changeTask(e)}
                className='form-control form-control-lg'></input>
            </div>
            <div className='col-auto'>
              <button
                onClick={updateTask}
                className='btn btn-lg btn-success mr-24'>Update</button>
              <button 
              onClick={cancelUpdate}
              className='btn btn-lg btn-warning '>Cancel</button>
            </div>
          </div>
          <br></br>

        </>
      ) : (
        <>
          {/* Add Task */}
          <div className='row'>
            <div className='col'>
              <input
                value={newTask}
                onChange={(e) => setNewTask(e.target.value)}
                className='form-control form-control-lg'></input>
            </div>
            <div className='col-auto'>
              <button
                onClick={addTask}
                className='btn btn-lg btn-info'>Add Task</button>
            </div>
          </div>
          <br></br>

        </>
      )}

      {/* Display ToDos */}
      {toDo && toDo.length ? '' : 'No Tasks...'}
      {toDo && toDo
        .sort((a, b) => a.id > b.id ? 1 : -1)
        .map((task, index) => {
          return (
            <React.Fragment keys={task.id}>
              <div className='col taskBg'>
                <div className={task.status ? 'done' : ''}>
                  <span className='taskNumber'>{index + 1}</span>
                  <span className='taskText'>{task.title}</span>
                </div>
                <div className='iconsWrap'>
                  <span title='Completed / Not Completed'
                    onClick={(e) => markDone(task.id)}>
                    <FaCheck></FaCheck>
                  </span>
                  {task.status ? null : (

                    <span title='Edit'
                      onClick={() => setUpdateData({
                        id: task.id,
                        title: task.title,
                        status: task.status ? true : false
                      })} >
                      <FaPen></FaPen>
                    </span>
                  )}
                  <span title='Delete'
                    onClick={() => deleteTask(task.id)}>
                    <FaTrash></FaTrash>
                  </span>
                </div>
              </div>
            </React.Fragment>
          )
        })
      }
    </div>
  );
}

export default App;